# frozen_string_literal: true

require_relative 'lib/declarative_policy/version'

Gem::Specification.new do |spec|
  spec.name          = 'declarative_policy'
  spec.version       = DeclarativePolicy::VERSION
  spec.authors       = ['Jeanine Adkisson', 'Alexis Kalderimis']
  spec.email         = ['akalderimis@gitlab.com']

  spec.summary       = 'An authorization library with a focus on declarative policy definitions.'
  spec.description   = <<~DESC
    This library provides an authorization framework with a declarative DSL

    With this library, you can write permission policies that are separate
    from business logic.

    This library is in production use at GitLab.com
  DESC
  spec.homepage      = 'https://gitlab.com/gitlab-org/ruby/gems/declarative-policy'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 3.0.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/ruby/gems/declarative-policy'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/ruby/gems/declarative-policy/-/blob/main/CHANGELOG.md'

  spec.metadata['rubygems_mfa_required'] = 'false' # rubocop:disable Gemspec/RequireMFA

  # Specify which files should be added to the gem when it is released.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    %w[
      *.gemspec
      lib/**/*.rb
      *.{md,txt}
      doc/**/*
    ].flat_map { |pattern| Dir.glob(pattern) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Development dependencies:
  spec.add_development_dependency 'benchmark-ips', '~> 2.12'
  spec.add_development_dependency 'gitlab-dangerfiles', '~> 3.8'
  spec.add_development_dependency 'gitlab-styles', '~> 10.1'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rspec', '~> 3.10'
  spec.add_development_dependency 'rspec-parameterized', '~> 1.0'
  spec.add_development_dependency 'rubocop-rails', '2.21.1'
end
